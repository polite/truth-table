#include <iostream>
#include <string>
#include <fstream>
#include <math.h>

using namespace std;

void erase(string& s, int index)
{
	for (int i = index; s[i] != '\0'; i++)
		s[i] = s[i + 1];
	s.erase(s.end() - 1);
}

char itos(int x)
{
	return (x + '0');
}

class Statement
{
	string name;
	int n, row, nparantez;
	bool** b;
	bool* p;
	int* paranpos;
	bool* result;

	bool paranparan(string s, int n)
	{
		int pi, pf, pii, pff;

		for (int i = 0; i < nparantez; i++)
		{
			pi = paranpos[i];
			for (pf = pi; s[pf] != ')'; pf++);

			s.insert(s.begin() + pf + 1, '*');

			string search;
			for (int j = 1; j < pf - pi; j++)
				search += s[pi + j];


			string ch = calculate(search, n) ? "T" : "F";
			s.replace(s.find("(" + search + ")*"), search.length() + 3, ch);
		}

		string ch = calculate(s, n) ? "T" : "F";
		s.replace(s.find(s), s.length(), ch);

		if (s == "T" || s == "(T)")
			return true;
		else if (s == "F" || s == "(F)")
			return false;//else if --> ~(...)
	}

	bool calculate(string s, int n)
	{
		int pp, qq;
		int i;

		while (s != "T" && s != "F")
		{
			if (s.length() < 3)
				return p_q(s, n);

			for (i = 0; s[i] != '\0'; i++)
				if ((s[i] >= 'a' && s[i] <= 'z') || s[i] == 'T' || s[i] == 'F' || s[i] == '~')
				{
					pp = i;
					if (s[i] == '~') i++;
					break;
				}
			for (i++; s[i] != '\0'; i++)
				if ((s[i] >= 'a' && s[i] <= 'z') || s[i] == 'T' || s[i] == 'F')
				{
					qq = i;
					break;
				}

			string search;
			for (int j = 0; j < qq - pp + 1; j++)
				search += s[pp + j];

			string ch = p_q(search, n) ? "T" : "F";
			s.replace(s.find(search), search.length(), ch);
		}
		if (s == "T")
			return true;
		return false;
	}

	bool p_q(string s, int n)
	{
		int k;
		bool p, q;
		char pp = 0, qq = 0;

		if (s.length() < 3)
		{
			if (s == "~T" || s == "F") return false;
			if (s == "~F" || s == "T") return true;
			//Find variable
			for (int i = 0; s[i] != '\0'; i++)
				if ((s[i] >= 'a' && s[i] <= 'z') || s[i] == 'T' || s[i] == 'F')
				{
					pp = s[i];
					break;
				}
			p = b[n][name.find(pp)];
			if (s[0] == '~') return !p;
			return p;
		}

		//Find variable
		for (int i = 0; s[i] != '\0'; i++)
			if ((s[i] >= 'a' && s[i] <= 'z') || s[i] == 'T' || s[i] == 'F')
			{
				pp = s[i];
				break;
			}

		for (int i = s.length(); i > 0; i--)
			if ((s[i] >= 'a' && s[i] <= 'z') || s[i] == 'T' || s[i] == 'F')
			{
				qq = s[i];
				break;
			}

		if (pp == 'T')
			p = true;
		else if (pp == 'F')
			p = false;
		else
			p = b[n][name.find(pp)];

		if (qq == 'T')
			q = true;
		else if (qq == 'F')
			q = false;
		else
			q = b[n][name.find(qq)];

		//Find ~p
		k = s.find(pp);
		erase(s, k);

		if (s[0] == '~')
		{
			p = !p;
			k = s.find("~");
			erase(s, k);
		}

		//Find ~q
		k = s.find(qq);
		erase(s, k);

		if (s[s.length() - 1] == '~')
		{
			q = !q;
			k = s.find("~");
			erase(s, k);
		}

		//Calculate
		if (s == "/\\" )
			return p & q;
		else if (s == "\\/")
			return p | q;
		else if (s == "->")
			return !p | q;
		else if (s == "<->")
			return (!p | q) & (p | !q);
	}

public:
	Statement(string s)
	{
		n = 0, nparantez = 0;
		// count variable
		for (int i = 0; s[i] != '\0'; i++)
			if (s[i] >= 'a' && s[i] <= 'z')
				if (name.find(s[i]) == string::npos)
				{
					n++;
					name += s[i];
				}

		for (int i = 0; s[i] != '\0'; i++)
			if (s[i] == '(')
				nparantez++;

		// create 2D array
		row = pow(2, n);
		b = new bool* [row];
		for (int i = 0; i < row; i++)
			b[i] = new bool[n];

		result = new bool[row];

		p = new bool[nparantez];
		paranpos = new int[nparantez];

		//initialization bools
		for (int i = 0; i < row; i++)
			for (int j = 0; j < n; j++)
				b[i][j] = !(i / (int)pow(2, n - j - 1) % 2);

		//paranpos[17] = 20;
		int j = 0;
		for (int i = 0; s[i] != '\0'; i++)
		{
			if (s[i] == '(')
			{
				paranpos[nparantez - j++ - 1] = i;
			}
		}

	}

	bool* show(string s, string sh)
	{

		/*for (int i = 0; i < n; i++)
			cout << name[i] << "\t";
		cout << sh << endl;

		for (int i = 0; i < 8 * n + s.length(); i++)
			cout << "_";
		cout << "\n";
		string space;
		int ttt;
		if (s.length() % 2 == 0) ttt = s.length() / 2 - 1;
		else ttt = s.length() / 2;
		for (int i = 0; i < ttt; i++)
			space += " ";*/

		////Truth table
		for (int i = 0; i < row; i++)
		{
		//	for (int j = 0; j < n; j++)
		//	{
		//		if (b[i][j]) cout << "T\t";
		//		else cout << "F\t";
		//	}

			//statement value
			char c = paranparan(s, i) ? 'T' : 'F';
			//cout << space << c;
			if (c == 'T') result[i] = true;
			else result[i] = false;

		//	cout << endl;
		}
		bool hasfalse = false;
		for (int i = 0; i < row; i++)
		{
			if (!result[i])
			{
				hasfalse = true;
				break;
			}
		}

		if (!hasfalse)
		{
			cout << "1";
		}
		else
			cout << "0";

		return result;
	}
};

int main()
{
	string statements;

	

	return 0;
}
